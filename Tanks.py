# Federico Vendramin 10th March 2018
# Pygame Tanks

import os
import pygame
from pygame.locals import *

from utils.ScreenProperties import ScreenProperties
from utils.TankPlayer import Player
from utils.Menu import Menu
from utils.EventHandler import EventHandler
from utils.Debug import DebugController, TextPrint
from utils.Sprites import PlayerSprite, WallSprite, BarrelSprite


screen_props = ScreenProperties(1200, 800)

# set_mode is different from open_window because it can create fullscreen games etc
screen = pygame.display.set_mode(screen_props.get_size(), HWSURFACE|DOUBLEBUF|RESIZABLE)
pygame.display.set_caption("Federico's Tank Game!")

paused = False
done = False
clock = pygame.time.Clock() # To manage screen update

pygame.init()
pygame.joystick.init()

#To print
textPrint = TextPrint()
debugController = DebugController(textPrint, screen)

joystick = None

if pygame.joystick.get_count() > 0:
    joystick = pygame.joystick.Joystick(0)
    joystick.init()

player = Player(0, joystick, screen_props, screen)
menu = Menu(screen_props, screen)

#Load background image
background_tile = pygame.image.load(os.path.join('media/PNG/Environment', 'dirt.png')).convert_alpha()
background_img = background_tile.copy()

for y in range(0, screen_props.get_height(), background_tile.get_height()):
    for x in range(0, screen_props.get_width(), background_tile.get_width()):
        screen.blit(background_tile, (x,y))

handler = EventHandler()


# Sprites
walls = pygame.sprite.Group()
players = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()

top_wall = WallSprite("black", 0, 0, 5, 5)
bottom_wall = WallSprite("black", 0, 0, 5, 5)
right_wall = WallSprite("black", 0, 0, 5, 5)
left_wall = WallSprite("black", 0, 0, 5, 5)


player_sprite = PlayerSprite(player.get_image(), screen_props.get_width()/2, screen_props.get_height()/2)
barrel_img = pygame.image.load(os.path.join('media/PNG/Tanks', 'barrelBlack.png')).convert()
player_barrel = BarrelSprite(barrel_img, player_sprite)
all_sprites.add(player_sprite)
all_sprites.add(player_barrel)


# Main Loop
while not handler.is_done():

    handler.update()

    if handler.is_paused():
        # Menu and Settings
        menu.draw(handler.get_selection())
    else:
        # Drawing Background
        for y in range(0, screen_props.get_height(), background_tile.get_height()):
            for x in range(0, screen_props.get_width(), background_tile.get_width()):
                screen.blit(background_tile, (x, y))

        # Game logic
        all_sprites.update(player)

        all_sprites.draw(screen)

    #Controller Debug Draw
    textPrint.reset()
    debugController.print_debug()

    # Update the screen with what we've drawn, to avoid flickering
    pygame.display.flip()

    # limit to 60 frames per second
    clock.tick(60)

pygame.quit()
