import pygame, os.path
from utils.Colors import *
from pygame.math import Vector2
import math

class WallSprite(pygame.sprite.Sprite):
    def __init__(self, color, x, y, width, height):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((width, height))
        self.image.fill(pygame.color.Color(color))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class TrailSprite(pygame.sprite.Sprite):
    def __init__(self, image, x, y, time, alpha=255):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.image.set_alpha(alpha)
        self.time_left = time
        self.alpha = alpha

    def set_image(self, image):
        self.image = image

    def set_transparency(self, alpha):
        self.image.set_alpha(alpha)
        self.alpha = alpha

    def update(self):
        self.time_left -= 1


class BarrelSprite(pygame.sprite.Sprite):
    def __init__(self, image, player):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.image.set_colorkey((255, 255, 255))
        rect_size = image.get_rect()
        self.rect = image.get_rect()

        self.rect.width = rect_size.width * 2
        self.rect.height = rect_size.height * 2
        self.original_image = image
        self.rect.x = player.rect.x
        self.rect.y = player.rect.y
        self.playerSprite = player

    def update(self, player):
        if player.direction == 'up':
            self.image = self.original_image
            self.rect.x = self.playerSprite.rect.x + self.playerSprite.image.get_rect()[2] / 2 - self.image.get_rect()[
                2] / 2
            self.rect.y = self.playerSprite.rect.y - self.playerSprite.image.get_rect()[3] / 2 + self.image.get_rect()[3] / 2

        if player.direction == 'down':
            self.image = pygame.transform.rotate(self.original_image, 180)
            self.rect.x = self.playerSprite.rect.x + self.playerSprite.image.get_rect()[2] / 2 - self.image.get_rect()[
                2] / 2
            self.rect.y = self.playerSprite.rect.y + self.playerSprite.image.get_rect()[3] / 2

        if player.direction == 'left':
            self.image = pygame.transform.rotate(self.original_image, 90)
            print("aa ", self.playerSprite.rect.x)
            self.rect.x = self.playerSprite.rect.x - self.original_image.get_rect()[3] + self.playerSprite.image.get_rect()[2] / 2
            self.rect.y = self.playerSprite.rect.y + self.playerSprite.image.get_rect()[3] / 2 - self.original_image.get_rect()[2] / 2

        if player.direction == 'right':
            self.image = pygame.transform.rotate(self.original_image, 90)
            self.rect.x = self.playerSprite.rect.x + self.playerSprite.image.get_rect()[2] / 2
            self.rect.y = self.playerSprite.rect.y + self.playerSprite.image.get_rect()[3] / 2 - self.original_image.get_rect()[2] / 2


class PlayerSprite(pygame.sprite.Sprite):
    def __init__(self, image, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.trails = pygame.image.load(os.path.join('media/PNG/Tanks', 'tracksSmall.jpg')).convert()
        self.trails.set_colorkey(WHITE)
        self.trails_horizontal = pygame.transform.rotate(self.trails, 90)
        self.trail_group = pygame.sprite.Group()
        self.trails_queue = []
        self.last_x = x
        self.last_y = y
        self.barrel = barrel_img = pygame.image.load(os.path.join('media/PNG/Tanks', 'barrelBlack.png')).convert()

    def update(self, player):
        # Read Controller
        left_joy_vertical_axis = player.joystick.get_axis(0)
        left_joy_horizontal_axis = player.joystick.get_axis(1)
        right_joy_vertical_axis = player.joystick.get_axis(2)
        right_joy_horizontal_axis = player.joystick.get_axis(3)
        triggers = player.joystick.get_axis(4)
        arrows = player.joystick.get_hat(0)


        # X Axis movement
        if (left_joy_vertical_axis > player.left_joy_vertical_deadzone):
            self.rect.x += left_joy_vertical_axis * player.rect_change_x
            player.direction = 'right'
        elif (left_joy_vertical_axis < -player.left_joy_vertical_deadzone):
            self.rect.x += left_joy_vertical_axis * player.rect_change_x
            player.direction = 'left'

        # Y Axis Movement
        elif (left_joy_horizontal_axis > player.left_joy_horizontal_deadzone):
            self.rect.y += left_joy_horizontal_axis * player.rect_change_y
            player.direction = 'down'
        elif (left_joy_horizontal_axis < -player.left_joy_horizontal_deadzone and player.rect_y > 0):
            self.rect.y += left_joy_horizontal_axis * player.rect_change_y
            player.direction = 'up'

        # Flashing horizontal
        if arrows[0] == -1 and player.flash_cd > player.flash_cooldown:
            self.rect.x -= player.flash_step
            player.flash_cd = 0
            player.direction = 'left'
        if arrows[0] == 1 and player.flash_cd > player.flash_cooldown:
            self.rect.x += player.flash_step
            player.flash_cd = 0
            player.direction = 'right'

        # Flashing vertical
        if arrows[1] == 1 and player.flash_cd > player.flash_cooldown:
            self.rect.y -= player.flash_step
            player.flash_cd = 0
            player.direction = 'up'
        if arrows[1] == -1 and player.flash_cd > player.flash_cooldown:
            self.rect.y += player.flash_step
            player.flash_cd = 0
            player.direction = 'down'

        # Check X bounds
        if self.rect.x > player.screen_props.get_width() - player.rect_x_size:
            self.rect.x = player.screen_props.get_width() - player.rect_x_size
        if self.rect.x < 0:
            self.rect.x = 0
        # Check Y bounds
        if self.rect.y > player.screen_props.get_height() - player.rect_y_size:
            self.rect.y = player.screen_props.get_height() - player.rect_y_size
        if self.rect.y < 0:
            self.rect.y = 0

        # Update cd
        if player.flash_cd < player.flash_cooldown + 1:
            player.flash_cd += 1

        # Draw TRAILS
        if player.direction == 'down':
            if self.rect.y > self.last_y + player.trails_distance:
                new_sprite = TrailSprite(self.trails, self.rect.x, self.last_y + 10, player.trails_duration)
                self.trails_queue.append(new_sprite)
                self.trail_group.add(new_sprite)

                self.last_y = self.last_y + player.trails_distance

        elif player.direction == 'right':
            if self.rect.x > self.last_x + player.trails_distance:
                new_sprite = TrailSprite(self.trails_horizontal, self.last_x + 10, self.rect.y, player.trails_duration)
                self.trails_queue.append(new_sprite)
                self.trail_group.add(new_sprite)

                self.last_x = self.last_x + player.trails_distance

        elif player.direction == 'up':
            if self.rect.y < self.last_y - player.trails_distance:
                new_sprite = TrailSprite(self.trails, self.rect.x, self.last_y + player.sprite_up.get_height() - 20,
                                                player.trails_duration)
                self.trails_queue.append(new_sprite)
                self.trail_group.add(new_sprite)

                self.last_y = self.last_y - player.trails_distance

        else:
            if self.rect.x < self.last_x - player.trails_distance:
                new_sprite = TrailSprite(self.trails_horizontal, self.rect.x + player.sprite.get_width(), self.rect.y, player.trails_duration)
                self.trails_queue.append(new_sprite)
                self.trail_group.add(new_sprite)

                self.last_x = self.last_x - player.trails_distance

        # For each of the queue
        # Fading trailings
        # Use SPRITES NOW (?)
        for i, trail in enumerate(self.trails_queue):
            if trail.time_left <= player.trails_duration:
                trail.set_transparency(255)
            if trail.time_left < 40:
                trail.set_transparency(150)
            if trail.time_left < 35:
                trail.set_transparency(90)
            if trail.time_left < 25:
                trail.set_transparency(30)
            if trail.time_left < 15:
                trail.set_transparency(10)

            trail.time_left -= 1
            if trail.time_left == 0:
                self.trails_queue.remove(trail)
                self.trail_group.remove(trail)

        self.trail_group.draw(player.screen)
        self.image = player.get_image()
