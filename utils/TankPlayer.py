#Federico Vendramin 11th Match 2018
#Pygame Tank Player Controller
from utils.Colors import *
import pygame, os.path
from utils.Sprites import BarrelSprite

#Handle the player controls
class Player:
    def __init__(self, playerID, joystick, screen_props, screen):
        #Player ID and Controller
        self.playerID = playerID
        self.joystick = joystick
        print(os.getcwd())

        self.sprite = pygame.image.load(os.path.join('media/PNG/Tanks', 'tankBlue.png')).convert()
        self.sprite_down = self.sprite
        self.sprite_right = pygame.transform.rotate(self.sprite_down, 90)
        self.sprite_up = pygame.transform.rotate(self.sprite_down, 180)
        self.sprite_left = pygame.transform.rotate(self.sprite_down, -90)
        self.direction = "up"
        self.trails = pygame.image.load(os.path.join('media/PNG/Tanks', 'tracksSmall.jpg')).convert()
        self.trails.set_colorkey(WHITE)
        self.trails_horizontal = pygame.transform.rotate(self.trails, 90)
        self.trails_queue = []
        self.trails_duration = 50
        self.trails_distance = 20
        self.moving_axis = 1


        #Screen for drawing and properties
        self.screen = screen
        self.screen_props = screen_props

        #Player Appearences and position
        self.rect_x = screen_props.get_width() / 2
        self.rect_y = screen_props.get_height() / 2
        self.last_x = self.rect_x
        self.last_y = self.rect_y
        self.rect_change_x = 5
        self.rect_change_y = 5
        self.rect_x_size = self.sprite.get_width()
        self.rect_y_size = self.sprite.get_height()
        self.left_joy_vertical_deadzone = 0.25
        self.left_joy_horizontal_deadzone = 0.25

        self.axes = self.joystick.get_numaxes()

        #Cooldowns
        self.flash_cd = 0
        self.flash_cooldown = 60
        self.trails_cd = 0
        self.trails_cooldown = 60

        #Ability Parameters
        self.flash_step = 75


    def update(self):
        #Read Controller
        self.left_joy_vertical_axis = self.joystick.get_axis(0)
        self.left_joy_horizontal_axis = self.joystick.get_axis(1)
        self.right_joy_vertical_axis = self.joystick.get_axis(2)
        self.right_joy_horizontal_axis = self.joystick.get_axis(3)
        self.triggers = self.joystick.get_axis(4)
        self.arrows = self.joystick.get_hat(0)

        #Start - Open Menu
        #if self.joystick.get_button(7):

        # X Axis movement
        if (self.left_joy_vertical_axis > self.left_joy_vertical_deadzone):
            self.rect_x += self.left_joy_vertical_axis * self.rect_change_x
            self.direction = 'right'
            self.trails_cd = 0
        elif (self.left_joy_vertical_axis < -self.left_joy_vertical_deadzone):
            self.rect_x += self.left_joy_vertical_axis * self.rect_change_x
            self.direction = 'left'
            self.trails_cd = 0

        # Y Axis Movement
        elif (self.left_joy_horizontal_axis > self.left_joy_horizontal_deadzone):
            self.rect_y += self.left_joy_horizontal_axis * self.rect_change_y
            self.direction = 'down'
            self.trails_cd = 0
        elif (self.left_joy_horizontal_axis < -self.left_joy_horizontal_deadzone and self.rect_y > 0):
            self.rect_y += self.left_joy_horizontal_axis * self.rect_change_y
            self.direction = 'up'
            self.trails_cd = 0

        # Flashing horizontal
        if self.arrows[0] == -1 and self.flash_cd > self.flash_cooldown:
            self.rect_x -= self.flash_step
            self.flash_cd = 0
            self.direction = 'left'
        if self.arrows[0] == 1 and self.flash_cd > self.flash_cooldown:
            self.rect_x += self.flash_step
            self.flash_cd = 0
            self.direction = 'right'

        # Flashing vertical
        if self.arrows[1] == 1 and self.flash_cd > self.flash_cooldown:
            self.rect_y -= self.flash_step
            self.flash_cd = 0
            self.direction = 'up'
        if self.arrows[1] == -1 and self.flash_cd > self.flash_cooldown:
            self.rect_y += self.flash_step
            self.flash_cd = 0
            self.direction = 'down'

        # Check X bounds
        if self.rect_x > self.screen_props.get_width() - self.rect_x_size:
            self.rect_x = self.screen_props.get_width() - self.rect_x_size
        if self.rect_x < 0:
            self.rect_x = 0
        # Check Y bounds
        if self.rect_y > self.screen_props.get_height() - self.rect_y_size:
            self.rect_y = self.screen_props.get_height() - self.rect_y_size
        if self.rect_y < 0:
            self.rect_y = 0

        #Update cd
        if self.flash_cd < self.flash_cooldown + 1:
            self.flash_cd += 1

    #Not used anymore!
    def draw(self):
        #Draw Tank with direction and trails
        #self.sprite_up.set_alpha(20)
        if self.direction == 'down':
            self.screen.blit(self.sprite_down, (self.rect_x, self.rect_y))
            while self.rect_y > self.last_y + self.trails_distance:
                self.trails_queue.append(["down", self.rect_x, self.last_y + 10 , self.trails_duration])
                self.last_y = self.last_y + self.trails_distance

        elif self.direction == 'right':
            self.screen.blit(self.sprite_right, (self.rect_x, self.rect_y))
            while self.rect_x > self.last_x + self.trails_distance:
                self.trails_queue.append(["right", self.last_x + 10, self.rect_y, self.trails_duration])
                self.last_x = self.last_x + self.trails_distance

        elif self.direction == 'up':
            self.screen.blit(self.sprite_up, (self.rect_x, self.rect_y))
            while self.rect_y < self.last_y - self.trails_distance:
                self.trails_queue.append(["up", self.rect_x, self.last_y + self.sprite_up.get_height() - 20, self.trails_duration])
                self.last_y = self.last_y - self.trails_distance

        else:
            self.screen.blit(self.sprite_left, (self.rect_x, self.rect_y))
            while self.rect_x < self.last_x - self.trails_distance:
                self.trails_queue.append(["left", self.rect_x + self.sprite.get_width(), self.rect_y, self.trails_duration])
                self.last_x = self.last_x - self.trails_distance

        if self.trails_cd < self.trails_cooldown:
            self.trails_cd += 1

        #For each of the queue
        # Fading trailings
        for trail in self.trails_queue:
            if (trail[0]== "down" or trail[0] == "up"):
                if trail[3] <= self.trails_duration:
                    self.trails.set_alpha(255)
                if trail[3] < 40:
                    self.trails.set_alpha(150)
                if trail[3] < 35:
                    self.trails.set_alpha(90)
                if trail[3] < 25:
                    self.trails.set_alpha(30)
                if trail[3] < 15:
                    self.trails.set_alpha(10)
                self.screen.blit(self.trails, (trail[1], trail[2]))
            else:
                if trail[3] <= self.trails_duration:
                    self.trails_horizontal.set_alpha(255)
                if trail[3] < 40:
                    self.trails_horizontal.set_alpha(150)
                if trail[3] < 35:
                    self.trails_horizontal.set_alpha(90)
                if trail[3] < 25:
                    self.trails_horizontal.set_alpha(30)
                if trail[3] < 15:
                    self.trails_horizontal.set_alpha(10)
                self.screen.blit(self.trails_horizontal, (trail[1], trail[2]))

            trail[3] -= 1
            if trail[3] == 0:
                self.trails_queue.remove(trail)

        print("AAA")
        self.screen.blit(self.barrel, (self.rect_x+15, self.rect_y-25))


    def get_image(self):
        if self.direction == 'down':
            return self.sprite_down
        elif self.direction == 'right':
            return self.sprite_right
        elif self.direction == 'up':
            return self.sprite_up
        else:
            return self.sprite_left

    def get_pos(self):
        return (self.rect_x, self.rect_y)