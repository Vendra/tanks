#Federico Vendramin 11 March 2018

#Debug class for Controllers and other stuff
#Print text to debug stuff

import pygame
from utils.Colors import *

class DebugController:
    def __init__(self, textPrint, screen):
        self.textPrint = textPrint
        self.screen = screen

    def print_debug(self):
        joystick_count = pygame.joystick.get_count()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            self.textPrint.print(self.screen, "Joystick {}".format(i))
            self.textPrint.indent()

            # Get the name from the OS for the controller/joystick
            name = joystick.get_name()
            self.textPrint.print(self.screen, "Joystick name: {}".format(name))

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()
            self.textPrint.print(self.screen, "Number of axes: {}".format(axes))
            self.textPrint.indent()

            for i in range(axes):
                axis = joystick.get_axis(i)
                self.textPrint.print(self.screen, "Axis {} value: {:>6.3f}".format(i, axis))
            self.textPrint.unindent()

            buttons = joystick.get_numbuttons()
            self.textPrint.print(self.screen, "Number of buttons: {}".format(buttons))
            self.textPrint.indent()

            for i in range(buttons):
                button = joystick.get_button(i)
                self.textPrint.print(self.screen, "Button {:>2} value: {}".format(i, button))
            self.textPrint.unindent()

            # Hat switch. All or nothing for direction, not like joysticks.
            # Value comes back in an array.
            hats = joystick.get_numhats()
            self.textPrint.print(self.screen, "Number of hats: {}".format(hats))
            self.textPrint.indent()

            for i in range(hats):
                hat = joystick.get_hat(i)
                self.textPrint.print(self.screen, "Hat {} value: {}".format(i, str(hat)))
            self.textPrint.unindent()

            self.textPrint.unindent()

# This is a simple class that will help us print to the screen
# It has nothing to do with the joysticks, just outputting the
# information.
class TextPrint:
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def print(self, screen, textString):
        textBitmap = self.font.render(textString, True, GREEN)
        screen.blit(textBitmap, [self.x, self.y])
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10