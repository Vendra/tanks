#Federico Vendramin 15th June 2018
#Pygame Menu
from utils.Colors import *
import pygame, os.path

class Menu:
    def __init__(self, screen_props, screen):
        # Screen for drawing and properties
        self.screen = screen
        self.screen_props = screen_props
        self.menu_width = 250
        self.menu_height = 170

        self.pointer_x = self.screen_props.get_width() / 2
        self.pointer_y = self.screen_props.get_height() / 2

    def draw(self, menu_selection):
        #Draw a Square in the center of the screen
        #for y in range(self.screen_props.get_height() - 20, self.screen_props.get_height() + 20):
        #    for x in range(self.screen_props.get_width() - 40, self.screen_props.get_width() + 40):
        #        self.screen.blit(x,y)
        pygame.draw.rect(self.screen, GREY, (self.screen_props.get_width()/2 - self.menu_width /2,
                                 self.screen_props.get_height()/2 - self.menu_height/2 ,
                                 self.menu_width, self.menu_height))

        self.pointer_x = self.screen_props.get_width() / 2
        self.pointer_y = self.screen_props.get_height() / 2

        self.write("RESUME", menu_selection == 0)
        self.write("EXIT", menu_selection == 1)

    def write(self,textString, selected):

        font = pygame.font.Font(None, 30)
        if selected == 1:
            textBitmap = font.render(textString, True, GREEN)
        else:
            textBitmap = font.render(textString, True, YELLOW)
        self.screen.blit(textBitmap, [ self.pointer_x - textBitmap.get_rect().width/2,
                                       self.pointer_y - self.menu_height/2 + 50])

        self.pointer_y += textBitmap.get_rect().height * 2



    class TextPrint:
        def __init__(self):
            self.reset()
            self.font = pygame.font.Font(None, 30)

        def print(self, screen, textString):
            textBitmap = self.font.render(textString, True, GREEN)
            screen.blit(textBitmap, [self.x, self.y])
            self.y += self.line_height

        def reset(self):
            self.x = self.screen_props
            self.y = 10
            self.line_height = 15

        def indent(self):
            self.x += 10

        def unindent(self):
            self.x -= 10