#Federico Vendramin 11 March 2018
#Class containing all screen properties

class ScreenProperties:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def get_size(self):
        return (self.width, self.height)

    def get_height(self):
        return self.height

    def get_width(self):
        return self.width

    def set_height(self, height):
        self.height = height

    def set_width(self, width):
        self.width = width

