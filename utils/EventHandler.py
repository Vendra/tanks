#Federico Vendramin 15th June 2018
#Event Handler

import pygame

class EventHandler:
    def __init__(self):
        self.paused = False
        self.menu_selection = 0
        self.menu_entry = 2
        self.done = False

    def is_paused(self):
        return self.paused

    def get_selection(self):
        return self.menu_selection

    def is_done(self):
        return self.done

    def update(self):
        # main events loop
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                print("User asked to quit.")
                self.done = True

            elif event.type == pygame.MOUSEBUTTONDOWN:
                print("User pressed a mouse button: ", event.pos)
                self.done = True

            # Pause the game
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 7:
                self.paused = True
            elif self.paused and event.type == pygame.JOYBUTTONDOWN \
                and (event.button == 7 or event.button == 1):
                self.menu_selection = 0
                self.paused = False

            #Menu Navigation
            elif self.paused and \
                    ((event.type == pygame.JOYAXISMOTION and event.axis == 1 and event.value >= 0.3)
                     or
                    (event.type == pygame.JOYHATMOTION and event.value == (0,-1))):
                if self.menu_selection < self.menu_entry - 1:
                    self.menu_selection += 1
            elif self.paused and \
                    ((event.type == pygame.JOYAXISMOTION and event.axis == 1 and event.value <= -0.3)
                     or
                    (event.type == pygame.JOYHATMOTION and event.value == (0, 1))):
                if self.menu_selection > 0:
                    self.menu_selection -= 1
            elif self.paused and event.type == pygame.JOYBUTTONDOWN and event.button == 0:
                if self.menu_selection == 0:
                    self.paused = False
                else:
                    self.done = True

            if event.type == pygame.VIDEORESIZE:
                # The main code that resizes the window:
                # (recreate the window with the new size)
                screen_props.set_width(event.w)
                screen_props.set_height(event.h)
                surface = pygame.display.set_mode(screen_props.get_size(), HWSURFACE | DOUBLEBUF | RESIZABLE)

                # Set Background Image
                for y in range(0, screen_props.get_height(), background_tile.get_height()):
                    for x in range(0, screen_props.get_width(), background_tile.get_width()):
                        screen.blit(background_tile, (x, y))

                #screen.blit(background_img, (0,0))
                pygame.display.flip()
