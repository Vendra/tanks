#Federico Vendramin
#Define some colors

BLACK = (0, 0, 0)
GREY = (55, 55, 55)
WHITE = (255, 255, 255)

GREEN = (0, 255, 0)

RED = (255, 0, 0)

LIGHT_BLUE = (30, 144, 255)
BLUE = (0, 0, 255)

YELLOW = (255, 255, 224)